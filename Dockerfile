FROM alpine:3

WORKDIR /backup/
ENV PATH="/backup:${PATH}"
COPY scripts/* ./
RUN chmod 750 cron.sh backup list fetch \
	&& apk add aws-cli bash coreutils gnupg mariadb-connector-c mysql-client xz

CMD ./cron.sh
